package ar.edu.unlu.poo.semana8;

public class RectanguloDesdeInterface implements IShape {
	
	int largo, ancho;
	
	RectanguloDesdeInterface(int largo, int ancho) {
		this.largo = largo;
		this.ancho = ancho;
	}

	@Override
	public void draw() {
		System.out.println("Dibuja rectangulo");
	}

	@Override
	public double area() {
		return (double) largo * ancho;
	}

}
