package ar.edu.unlu.poo.semana8;

public class TestAbstractaVsInterfaces {

	public static void main(String[] args) {
		
		/**
		 * Creamos objetos de clases que extienden de la abstracta 
		 */
		
        Shape rect = new RectanguloDesdeAbstracta(2, 3, "Rectangle");
        Shape circle = new CirculoDesdeAbstracta(2, "Circle");
        
        System.out.println("Area of rectangle: " + rect.area());
        System.out.println("Area of circle: " + circle.area());
        
        /* Reusa método heredado de la abstracta, es el mismo código */
        rect.moverA(1, 2);
        circle.moverA(2, 4);
        
        /**
         * Creamos objetos que implementan la interfaz
         */
        IShape rect = new RectanguloDesdeInterface(2, 3);
        IShape circle = new CirculoDesdeInterface(2);
        
        System.out.println("Area of rectangle: " + rect.area());
        System.out.println("Area of circle: " + circle.area());
	}
}
