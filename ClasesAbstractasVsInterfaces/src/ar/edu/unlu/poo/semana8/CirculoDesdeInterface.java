package ar.edu.unlu.poo.semana8;

public class CirculoDesdeInterface implements IShape {

	double pi = 3.14;
    int radius;
 
    CirculoDesdeInterface(int radius) { 
    	this.radius = radius;
	}
 
    @Override
    public void draw() {
        System.out.println("Se dibujó el círculo");
    }
 
    @Override
    public double area() {
        return (double)((pi * radius * radius));
    }

}
