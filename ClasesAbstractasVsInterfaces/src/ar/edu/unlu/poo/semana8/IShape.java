package ar.edu.unlu.poo.semana8;

public interface IShape {

	void draw();
    double area();
}
