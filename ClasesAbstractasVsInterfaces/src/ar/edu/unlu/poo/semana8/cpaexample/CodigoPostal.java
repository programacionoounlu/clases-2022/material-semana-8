package ar.edu.unlu.poo.semana8.cpaexample;

public class CodigoPostal {
	PrefijoCodigoPostalArgentino prefijo;
	SufijoCodigoPostalArgentino sufijo;
	
	CodigoPostal(PrefijoCodigoPostalArgentino prefijo, SufijoCodigoPostalArgentino sufijo) {
		this.prefijo = prefijo;
		this.sufijo = sufijo;
	}
	
	public String getCodigoPostal() {
		return this.prefijo.getPrefijo() + this.sufijo.getSufijo();
	}

}
