package ar.edu.unlu.poo.semana8.cpaexample;

public class SufijoCodigoPostalArgentino {
	String caraDeManzana;
	
	SufijoCodigoPostalArgentino(String caraDeManzana) {
		this.caraDeManzana = caraDeManzana;
	}
	
	public String getSufijo() {
		return this.caraDeManzana;	
	}
}
