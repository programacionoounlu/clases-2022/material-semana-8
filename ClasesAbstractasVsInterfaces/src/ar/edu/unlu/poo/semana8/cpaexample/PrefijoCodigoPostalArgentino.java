package ar.edu.unlu.poo.semana8.cpaexample;

public class PrefijoCodigoPostalArgentino {
	char provincia;
	int localidad;
	
	PrefijoCodigoPostalArgentino(char provincia, int localidad) {
		this.provincia = provincia;
		this.localidad = localidad;
	}
	
	public String getPrefijo() {
		return this.provincia + String.valueOf(this.localidad);	
	}

}
