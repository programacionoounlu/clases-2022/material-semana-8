package ar.edu.unlu.poo.semana8;

public abstract class Shape {
	String nombre = " ";
	
	Shape(String nombre) {
		this.nombre = nombre;
	}
	
	public void moverA(int x, int y) {
		System.out.println(this.nombre + " " 
				+ " fue movido a "
				+ " x = " + x + " , y = " + y);
	}
	
	abstract public double area();
    abstract public void draw();
}
