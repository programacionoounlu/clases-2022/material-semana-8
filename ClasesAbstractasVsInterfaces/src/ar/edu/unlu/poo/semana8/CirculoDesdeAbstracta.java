package ar.edu.unlu.poo.semana8;

public class CirculoDesdeAbstracta extends Shape {

    double pi = 3.14;
    int radius;

    CirculoDesdeAbstracta(int radius, String name)
    {
        super(name);
        this.radius = radius;
    }
 
    @Override
    public void draw() {
        System.out.println("Se dibujó el círculo");
    }
 
    @Override
    public double area() {
        return (double)((pi * radius * radius));
    }

}
