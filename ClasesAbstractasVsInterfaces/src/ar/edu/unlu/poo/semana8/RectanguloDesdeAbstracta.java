package ar.edu.unlu.poo.semana8;

public class RectanguloDesdeAbstracta extends Shape {
	
	int largo, ancho;
	
	RectanguloDesdeAbstracta(int largo, int ancho, String nombre) {
		super(nombre);
		this.largo = largo;
		this.ancho = ancho;
	}

	@Override
	public double area() {
		return (double) largo * ancho;
	}

	@Override
	public void draw() {
		System.out.println("Dibuja rectangulo");
	}

}
